using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica_en_clase
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1 Ingresar numeros positivos  sumarlos\n" +
                "2 Numeros del 1 al 10 con do while\n" +
                "3 Primeros 20 numeros pares apartir del producto de 10*10\n" +
                "4 Letras en mayusculas desde Z hasta A\n");
            short opc = short.Parse(Console.ReadLine());

            Console.Clear();
            switch (opc)
            {
                case 1:
                    int repetidor = 1;
                    int suma = 0;

                    while (repetidor != 0 || repetidor > 0)
                    {
                        Console.WriteLine("Ingrese cualquier numero excepto el 0 a no ser que quiera terminar");
                        repetidor = int.Parse(Console.ReadLine());
                        suma = suma + repetidor;
                    }

                    Console.WriteLine("la suma de los numeros anteriores es");
                    Console.WriteLine(suma);
                    Console.ReadKey();
                    break;

                case 2:

                    short num = 1;

                    do
                    {
                        Console.WriteLine(num);
                        num++;
                    }

                    while (num <= 10);
                    break;

                case 3:

                    int n = (10 * 10);


                    for (int i=1; i <= 10; i++)
                    {
                        n = n + 2;
                        Console.WriteLine(n);
                    }

                    break;

                case 4:
                    char letra = 'Z';

                    while (letra >= 'A')
                    {
                        Console.Write("\n"+letra);

                        letra--;

                    }

                    Console.ReadLine();
                    break;
            }

            Console.ReadKey();
        }

    }
}
